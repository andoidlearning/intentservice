package com.example.intentservice

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnStart : Button = findViewById(R.id.btnStart)
        val btnStop : Button = findViewById(R.id.btnStop)
        val tvService : TextView = findViewById(R.id.tvServiceinfo)

        btnStart.setOnClickListener {
            Intent(this, MyIntentService::class.java).also {
                startService(it)
                tvService.text = "Service running"
            }
        }

        btnStop.setOnClickListener {
            MyIntentService.stopService()
            tvService.text = "Service stopped"
        }
    }
}